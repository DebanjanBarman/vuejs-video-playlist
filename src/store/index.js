import Vue from 'vue'
import Vuex from 'vuex'
import { URL } from '../../config'

const axios = require('axios')

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    course: {},
    activeVideo: ''
  },
  mutations: {
    SET_COURSE: (state, course) => {
      state.course = course
    },
    SET_INITIAL_VIDEO: (state, video) => {
      state.activeVideo = video
    },
    SET_ACTIVE_VIDEO: (state, url) => {
      state.activeVideo = url
    }
  },
  actions: {
    getVideo: async context => {
      const resp = await axios.get(URL)
      const { course } = resp.data.data

      context.commit('SET_COURSE', course)

      const initialVideo = course.modules[0].clips[0].playerUrl
      context.commit('SET_INITIAL_VIDEO', initialVideo)
    },
    changeActiveVideo: (context, payload) => {
      context.commit('SET_ACTIVE_VIDEO', payload)
    }
  },
  getters: {
    courseName: state => {
      return state.course.title
    },
    activeVideo: state => state.activeVideo,
    courseAuthor: state => state.course.author,
    courseModules: state => state.course.modules
  },
  modules: {}
})
